<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    protected $table = 'sekolahs';
    protected $primaryKey = 'id_sekolah';
    protected $fillable = [
        'admin_id',
        'kecamatan_id',
        'nama_sekolah',
        'alamat_sekolah',
        'lokasi',
        'keterangan',
        'status'
    ];

    public function kecamatan(){
        return $this->belongsTo('App\Kecamatan', 'kecamatan_id');
    }
}
