<?php

namespace App\Http\Controllers\Admin;

use Session;
use Validator;
use App\Guru;
use App\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuruController extends Controller
{
    public function tambahGuru(){
        if (Session::get('logged_in') == TRUE) {
            $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
            return view('admin.guru.tambah', compact('admin'));
        }else{
            return redirect('login');
        }
    }

    public function index(){
        if (Session::get('logged_in') == TRUE) {
            $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
            $gurus = Guru::all();
            return view('admin.guru.index', compact('admin', 'gurus'));
        }else{
            return redirect('login');
        }
    }

    public function simpanGuru(Request $r){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'nip' => 'required',
            'username' => 'required',
            'password' => 'required',
            'jabatan' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $query = Guru::where('nip',$r->nip)->first();
            if ($query == null) {
                $guru = Guru::create([
                    'nama' => $r->nama,
                    'nip' => $r->nip,
                    'username' => $r->username,
                    'password' => bcrypt($r->password),
                    'jabatan' => $r->jabatan,
                    'status' => 1,
                    'admin_id' => Session::get('id_admin')
                ]);
                toastSuccess('Data berhasil ditambahkan!');
                return redirect()->back();
            }else{
                toastError('NIP Sudah digunakan!');
                return redirect()->back()->withInput();
            }
        }
    }

    public function changePassword(Request $r, $id){
        $guru = Guru::where('id_guru',$id)->update([
            'password' => bcrypt($r->password)
        ]);
        toastSuccess('Password berhasil diubah!');
        return redirect()->back();
    }

    public function updateGuru(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama' => 'required',
            'nip' => 'required',
            'username' => 'required',
            'jabatan' => 'required',
            'status' => 'required'
        ]);

        if($validator->fails()){
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $guru = Guru::where('id_guru',$id)->update([
                'nama' => $r->nama,
                'nip' => $r->nip,
                'username' => $r->username,
                'jabatan' => $r->jabatan,
                'status' => $r->status
            ]);
            toastSuccess('Data berhasil ditambahkan!');
            return redirect()->back();
        }
    }

    public function deleteGuru($id){
        $guru = Guru::where('id_guru',$id)->delete();
        return redirect()->back();
    }
    
}
