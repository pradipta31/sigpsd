<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        if (Session::get('logged_in') == TRUE) {
            $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
            return view('admin.dashboard', compact('admin'));
        }else{
            return redirect('login');
        }
    }
}
