<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Session;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index(){
        if (Session::get('logged_in') == TRUE) {
            $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
            return view('admin.profile', compact('admin'));
        }else{
            return redirect('login');
        }
    }

    public function updateProfile(Request $r,$id){
        $validator = Validator::make($r->all(),[
            'nama' => 'required',
            'nip' => 'required',
            'username' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password != null) {
                $admin = Admin::where('id_admin',$id)->update([
                    'nama' => $r->nama,
                    'nip' => $r->nip,
                    'username' => $r->username,
                    'password' => bcrypt($r->password)
                ]);
                toastInfo('Profile anda berhasil diubah');
                return redirect()->back();
            }else{
                $admin = Admin::where('id_admin',$id)->update([
                    'nama' => $r->nama,
                    'nip' => $r->nip,
                    'username' => $r->username
                ]);
                toastInfo('Profile anda berhasil diubah');
                return redirect()->back();
            }
        }
    }
}
