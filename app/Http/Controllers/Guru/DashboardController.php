<?php

namespace App\Http\Controllers\Guru;

use Session;
use App\Guru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        if (Session::get('log_in') == TRUE) {
            $guru = Guru::where('id_guru',Session::get('id_guru'))->first();
            return view('guru.dashboard', compact('guru'));
        }else{
            return redirect('guru');
        }
    }
}
