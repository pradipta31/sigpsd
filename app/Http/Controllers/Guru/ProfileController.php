<?php

namespace App\Http\Controllers\Guru;

use Validator;
use Session;
use App\Guru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index(){
        if (Session::get('log_in') == TRUE) {
            $guru = Guru::where('id_guru',Session::get('id_guru'))->first();
            return view('guru.profile', compact('guru'));
        }else{
            return redirect('guru');
        }
    }

    public function updateProfile(Request $r,$id){
        $validator = Validator::make($r->all(),[
            'nama' => 'required',
            'nip' => 'required',
            'username' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password != null) {
                $guru = Guru::where('id_guru',$id)->update([
                    'nama' => $r->nama,
                    'nip' => $r->nip,
                    'username' => $r->username,
                    'password' => bcrypt($r->password)
                ]);
                toastInfo('Profile anda berhasil diubah');
                return redirect()->back();
            }else{
                $guru = Guru::where('id_guru',$id)->update([
                    'nama' => $r->nama,
                    'nip' => $r->nip,
                    'username' => $r->username
                ]);
                toastInfo('Profile anda berhasil diubah');
                return redirect()->back();
            }
        }
    }
}
