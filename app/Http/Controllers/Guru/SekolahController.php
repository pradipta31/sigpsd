<?php

namespace App\Http\Controllers\Guru;

use Session;
use Validator;
use App\Guru;
use App\Kecamatan;
use App\Sekolah;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SekolahController extends Controller
{
    public function tambahSekolah(){
        if (Session::get('log_in') == TRUE) {
            $guru = Guru::where('id_guru',Session::get('id_guru'))->first();
            $kecamatans = Kecamatan::all();
            return view('guru.sekolah.tambah', compact('guru','kecamatans'));
        }else{
            return redirect('guru');
        }
    }

    public function simpanSekolah(Request $r){
        $validator = Validator::make($r->all(), [
            'kecamatan_id' => 'required',
            'nama_sekolah' => 'required',
            'alamat_sekolah' => 'required',
            'nama_kepsek' => 'required',
            'lokasi' => 'required',
            'keterangan' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $query = Sekolah::where('nama_sekolah',$r->nama_sekolah)->first();
            if ($query == null) {
                $guru = Sekolah::create([
                    'kecamatan_id' => $r->kecamatan_id,
                    'nama_sekolah' => $r->nama_sekolah,
                    'alamat_sekolah' => $r->alamat_sekolah,
                    'nama_kepsek' => $r->nama_kepsek,
                    'lokasi' => $r->lokasi,
                    'keterangan' => $r->keterangan,
                    'status' => 1,
                    'admin_id' => Session::get('id_guru')
                ]);
                toastSuccess('Data berhasil ditambahkan!');
                return redirect()->back();
            }else{
                toastError('Nama sekolah sudah ada!');
                return redirect()->back()->withInput();
            }
        }
    }

    public function index(){
        if (Session::get('log_in') == TRUE) {
            $guru = Guru::where('id_guru',Session::get('id_guru'))->first();
            $kecamatans = Kecamatan::all();
            $schools = Sekolah::all();
            return view('guru.sekolah.index', compact('guru', 'kecamatans', 'schools'));
        }else{
            return redirect('guru');
        }
    }

    public function updateSekolah(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'kecamatan_id' => 'required',
            'nama_sekolah' => 'required',
            'alamat_sekolah' => 'required',
            'nama_kepsek' => 'required',
            'lokasi' => 'required',
            'keterangan' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $guru = Sekolah::where('id_sekolah',$id)->update([
                'kecamatan_id' => $r->kecamatan_id,
                'nama_sekolah' => $r->nama_sekolah,
                'alamat_sekolah' => $r->alamat_sekolah,
                'nama_kepsek' => $r->nama_kepsek,
                'lokasi' => $r->lokasi,
                'keterangan' => $r->keterangan,
                'status' => $r->status
            ]);
            toastSuccess('Data sekolah berhasil diubah!');
            return redirect()->back();
        }
    }

    public function destroy($id){
        $sekolah = Sekolah::where('id_sekolah',$id)->delete();
        toastSuccess('Data sekolah berhasil dihapus!');
        return redirect()->back();
    }
}
