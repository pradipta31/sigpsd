<?php

namespace App\Http\Controllers\Guru;

use Session;
use App\Guru;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function index(){
        return view('guru.login');
    }

    public function login(Request $r){
        $username = $r->username;
        $password = $r->password;
        $data = Guru::where('username', $username)->first();
        if ($data) {
            if ($data->status == '1') {
                if(Hash::check($password,$data->password)){ 
                    Session::put('id_guru', $data->id_guru);
                    Session::put('log_in',TRUE);
                    return redirect(url('guru/home'));
                }       
                else{
                    Session::flash('error', 'Login Gagal! Username atau Password Salah!');
                    return redirect()->back()->withInput();
                }
            }else{
                Session::flash('error', 'Login Gagal! Guru nonaktif!');
                return redirect()->back()->withInput();
            }
        }else{
            Session::flash('error', 'Login Gagal! Username atau Password Salah!');
            return redirect()->back()->withInput();
        }
    }
}
