<?php

namespace App\Http\Controllers\Frontend;

use DB;
use App\Kecamatan;
use App\Sekolah;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SekolahController extends Controller
{
    public function show($id_sekolah){
        $sekolah = Sekolah::where('id_sekolah',$id_sekolah)->first();
        $districts = Kecamatan::all();
        return view('result', compact('sekolah','districts'));
    }

    public function daftarSekolah(){
        $districts = Kecamatan::all();
        return view('daftar-sekolah', compact('districts'));
    }

    public function search(Request $r) {
        $search = $r->id_kecamatan;
        $results = [];
        if($search){
            $districts = Kecamatan::all();
            $kecamatan = Kecamatan::where('id_kecamatan',$search)->first();
            $results = Sekolah::where('kecamatan_id', 'LIKE', '%'. $search .'%')
            ->get();
            return view('hasil-sekolah')->with([
                'kecamatan' => $kecamatan,
                'search' => $search,
                'districts' => $districts,
                'results' => $results
            ]);
        }
    }

    public function cari(Request $r){
        $search = $r->cari;
        $districts = Kecamatan::all();
        if ($search) {
            $sekolah = Sekolah::where('nama_sekolah', 'LIKE', '%'.$search.'%')
            ->first();
            return view('result', compact('sekolah', 'districts'));
        }
    }
}
