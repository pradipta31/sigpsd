<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSekolahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sekolahs', function (Blueprint $table) {
            $table->bigIncrements('id_sekolah');
            $table->unsignedBigInteger('admin_id')
                  ->foreign('admin_id')->references('id_admin')->on('admins')
                  ->foreign('admin_id')->references('id_guru')->on('gurus');
            $table->unsignedBigInteger('kecamatan_id')
                  ->foreign('kecamatan_id')->references('id_kecamatan')->on('kecamatans');
            $table->string('nama_sekolah');
            $table->text('alamat_sekolah');
            $table->text('lokasi');
            $table->text('keterangan');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sekolahs');
    }
}
