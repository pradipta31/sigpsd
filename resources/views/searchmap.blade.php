<style>
  #footer .footer-newsletter form input[type="text"] {
    border: 0;
    padding: 4px 8px;
    width: calc(100% - 100px);
  }
</style>
<div class="footer-newsletter" data-aos="fade-up">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <h4>Cari Sekolah</h4>
          <p>Cari sekolah berdasarkan nama. (Cth: SDN 1 Gilimanuk)</p>
          <form action="{{url('cari')}}" method="GET" enctype="multipart/form-data">
            <input type="text" name="cari"><input type="submit" value="Cari">
          </form>
        </div>
      </div>
    </div>
</div>