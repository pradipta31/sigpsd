@extends('home')
@section('confront')
    <style>
        .btn_style{
            padding: 7px 25px;
            border-radius: 28px;
            border: 1px;
            background-color: #5777ba;
            color: azure;
            text-shadow: 1px;
        }
    </style>
    <section id="pricing" class="pricing">
        <div class="container">
            <hr>
            <div class="section-title">
                <h2>Daftar Sekolah</h2>
                <p>Cari sekolah berdasarkan kecamatan.</p>
                
                    <form action="{{url('search')}}" method="GET">
                        <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <select name="id_kecamatan" class="form-control">
                                    @foreach ($districts as $district)
                                        <option value="{{$district->id_kecamatan}}" {{$district->id_kecamatan == $kecamatan->id_kecamatan ? 'selected' : ''}}>{{$district->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <input type="submit" value="Cari" class="btn_style">
                        </div>
                        <div class="col-lg-4"></div>
                        </div>
                    </form>
            </div>
  
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="box">
                        <div class="section-title text-center">
                            <h2>Kecamatan: {{$kecamatan->nama}}</h2>
                        </div>
                        @php
                            $hasil = \App\Sekolah::where('kecamatan_id',$kecamatan->id_kecamatan)->first();
                        @endphp
                        @if($hasil == null)
                            Data not found!
                        @else
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Sekolah</th>
                                            <th>Alamat Sekolah</th>
                                            <th>Keterangan</th>
                                            <th>Lokasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($results as $item)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{$item->nama_sekolah}}</td>
                                                <td>{{$item->alamat_sekolah}}</td>
                                                <td>{{$item->keterangan}}</td>
                                                <td>
                                                    <a href="{{url('sekolah/'.$item->id_sekolah)}}" style="display:unset">
                                                    Lokasi
                                                    <i class="bx bx-right-arrow-circle"></i>
                                                </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
  
        </div>
      </section>

    <footer id="footer">
        @include('searchmap')
    </footer>
@endsection