@extends('admin.layouts.master',['activeMenu' => 'data-sekolah'])
@section('title', 'Data Sekolah')
@section('breadcrumb', 'Data Sekolah')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{url('admin/sekolah/tambah')}}" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i>
                        Tambah Sekolah
                    </a>

                </div>
                <div class="card-body">
                    <table id="tableSekolah" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kecamatan</th>
                                <th>Nama</th>
                                <th>Kepala Sekolah</th>
                                <th>Alamat</th>
                                <th>Status</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($schools as $sekolah)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$sekolah->kecamatan->nama}}</td>
                                    <td>{{$sekolah->nama_sekolah}}</td>
                                    <td>{{$sekolah->nama_kepsek}}</td>
                                    <td>{{$sekolah->alamat_sekolah}}</td>
                                    <td>
                                        @if($sekolah->status == 1)
                                            <span class="lb success">Aktif</span>
                                        @else
                                            <span class="lb warning">Non Aktif</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#lihatSekolah{{$sekolah->id_sekolah}}">
                                            <i class="fa fa-map-marker-alt"></i>
                                            Lihat
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editSekolah{{$sekolah->id_sekolah}}">
                                            <i class="fa fa-pencil"></i>
                                            Edit
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="deleteSekolah('{{$sekolah->id_sekolah}}')">
                                            <i class="fa fa-trash"></i>
                                            Hapus
                                        </a>
                                    </td>
                                </tr>

                                <div class="modal fade bd-example-modal-lg" id="lihatSekolah{{$sekolah->id_sekolah}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="exampleModalLabel">Data Sekolah : {{$sekolah->nama_sekolah}}</h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="responsive-map-container">
                                                            <iframe src="{{$sekolah->lokasi}}" width="600" height="450" frameborder="0" style="border:0"></iframe>
                                                        </div>
                                                        <hr>
                                                        <div class="form-group">
                                                            {{$sekolah->keterangan}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade" id="editSekolah{{$sekolah->id_sekolah}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="exampleModalLabel">Ubah Data Sekolah : {{$sekolah->nama_sekolah}}</h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
            
                                            <form action="{{url('admin/sekolah/'.$sekolah->id_sekolah.'/edit')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="_method" value="put">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Kecamatan</label>
                                                                <select name="kecamatan_id" class="form-control" value="{{old('kecamatan_id')}}">
                                                                    @foreach($kecamatans as $kecamatan)
                                                                        <option value="{{$kecamatan->id_kecamatan}}">{{$kecamatan->nama}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Sekolah</label>
                                                                <input type="text" class="form-control" placeholder="Masukan Nama Sekolah" name="nama_sekolah" value="{{$sekolah->nama_sekolah}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Kepala Sekolah</label>
                                                                <input type="text" class="form-control" placeholder="Masukan Nama Kepala Sekolah" name="nama_kepsek" value="{{$sekolah->nama_kepsek}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Alamat Sekolah</label>
                                                                <input type="text" class="form-control" name="alamat_sekolah" value="{{$sekolah->alamat_sekolah}}" placeholder="Masukan alamat sekolah">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Lokasi</label>
                                                                <input type="text" class="form-control" name="lokasi" value="{{$sekolah->lokasi}}" placeholder="Masukan lokasi">
                                                                <small>NB: Copy embed map pada google maps.</small>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Keterangan</label>
                                                                <textarea name="keterangan" class="form-control" cols="30" rows="5">{{$sekolah->keterangan}}</textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Status</label>
                                                                <select name="status" id="" class="form-control" value="{{$sekolah->status}}">
                                                                    <option value="1" {{$sekolah->status == 1 ? 'selected' : ''}}>Aktif</option>
                                                                    <option value="0" {{$sekolah->status == 0 ? 'selected' : ''}}>Non Aktif</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('backend/bootbox/bootbox.min.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#tableSekolah').DataTable();
        });

        function lihatSekolahModal(){
            $('#lihatSekolah').modal('show');
        }

        function editSekolahModal(){
            $('#editSekolah').modal('show');
        }

        function deleteSekolah(id){
            swal({
                title: "Anda yakin?",
                text: "Anda tidak dapat mengembalikan data yang sudah terhapus!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data sekolah yang anda pilih berhasil dihapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/sekolah/delete')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection