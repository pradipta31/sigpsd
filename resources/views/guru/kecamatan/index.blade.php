@extends('admin.layouts.master',['activeMenu' => 'data-kecamatan'])
@section('title', 'Data Kecamatan')
@section('breadcrumb', 'Data Kecamatan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="#" class="btn btn-primary btn-sm" data-target="#tambahKecamatan" data-toggle="modal">
                        <i class="fa fa-plus"></i>
                        Tambah Kecamatan
                    </a>

                    <div class="modal fade" id="tambahKecamatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">Tambah Kecamatan Baru</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <form action="{{url('admin/kecamatan/tambah')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Nama Kecamatan</label>
                                                    <input type="text" class="form-control" name="nama" placeholder="Masukan Nama Kecamatan" value="{{ old('nama') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Keterangan</label>
                                                    <textarea name="keterangan" class="form-control" cols="30" rows="10">{{old('keterangan')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="tableKecamatan" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Kecamatan</th>
                                <th>Keterangan</th>
                                <th>Tanggal</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($districts as $district)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$district->nama}}</td>
                                    <td>{{$district->keterangan}}</td>
                                    <td>
                                        {{$district->created_at->format('d-m-Y')}}
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editKecamatan{{$district->id_kecamatan}}">
                                            <i class="fa fa-pencil"></i>
                                            Edit
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="deleteKecamatan('{{$district->id_kecamatan}}')">
                                            <i class="fa fa-trash"></i>
                                            Hapus
                                        </a>
                                    </td>
                                </tr>

                                <div class="modal fade" id="editKecamatan{{$district->id_kecamatan}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="exampleModalLabel">Ubah Data Kecamatan : {{$district->nama}}</h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
            
                                            <form action="{{url('admin/kecamatan/'.$district->id_kecamatan.'/edit')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="_method" value="put">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label">Nama Kecamatan</label>
                                                                <input type="text" class="form-control" name="nama" placeholder="Masukan Nama Kecamatan" value="{{$district->nama}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label">Keterangan</label>
                                                                <textarea name="keterangan" class="form-control" cols="30" rows="10">{{$district->keterangan}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form class="hidden" action="" method="post" id="formPassword">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="password" value="" id="password">
    </form>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('backend/bootbox/bootbox.min.js')}}"></script>
    <script type="text/javascript">
        function tambahKec(){
            $('#tambahKecamatan').modal('show');
        }

        $(function () {
            $('#tableKecamatan').DataTable();
        });

        function editKecamatanModal(){
            $('#editKecamatan').modal('show');
        }

        function deleteKecamatan(id){
            swal({
                title: "Anda yakin?",
                text: "Anda tidak dapat mengembalikan data yang sudah terhapus!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data kecamatan yang anda pilih berhasil dihapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/kecamatan/delete')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection