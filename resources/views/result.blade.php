@extends('home')
@section('confront')
    <section id="pricing" class="pricing">
        <div class="container">
          <hr>
          @if($sekolah == null)
          <div class="section-title">
            
          </div>
          <div class="row no-gutters" style="margin-top: 5%">
            Maaf data yang anda cari tidak ditemukan!
  
          </div>
          @else
            <div class="section-title">
              <h2>{{$sekolah->nama_sekolah}}</h2>
              <p>{{$sekolah->keterangan}}</p>
            </div>
    
            <div class="row no-gutters">
    
              <div class="box" data-aos="fade-right">
                  <div class="responsive-map-container">
                      <iframe src="{{$sekolah->lokasi}}" width="1000" height="450" frameborder="0" style="border:0"></iframe>
                  </div>
              </div>
    
            </div>
          @endif
  
        </div>
      </section>

    <footer id="footer">
        @include('searchmap')
    </footer>
@endsection