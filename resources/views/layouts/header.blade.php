<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href="{{url('/')}}">SIGPSD</a></h1>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="{{url('/')}}">Home</a></li>
          <li><a href="{{url('daftar-sekolah')}}">Daftar Sekolah</a></li>
          {{-- <li><a href="{{url('cari-sekolah')}}">Cari Sekolah</a></li> --}}

          <li class="get-started"><a href="{{url('daftar-sekolah')}}">Sekolah</a></li>
        </ul>
      </nav>

    </div>
</header>