@extends('home')
@section('confront')
    <style>
        .btn_style{
            padding: 7px 25px;
            border-radius: 28px;
            border: 1px;
            background-color: #5777ba;
            color: azure;
            text-shadow: 1px;
        }
    </style>
    <section id="pricing" class="pricing">
        <div class="container">
            <hr>
            <div class="section-title">
                <h2>Daftar Sekolah</h2>
                <p>Cari sekolah berdasarkan kecamatan.</p>
                
                    <form action="{{url('search')}}" method="GET">
                        <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <select name="id_kecamatan" class="form-control">
                                    @foreach ($districts as $district)
                                        <option value="{{$district->id_kecamatan}}">{{$district->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <input type="submit" value="Cari" class="btn_style">
                        </div>
                        <div class="col-lg-4"></div>
                        </div>
                    </form>
            </div>
  
        </div>
      </section>

    <footer id="footer">
        @include('searchmap')
    </footer>
@endsection