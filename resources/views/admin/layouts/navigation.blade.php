<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <img src="{{asset('images/logo/logo2.png')}}" alt="AdminLTE Logo" class="brand-image"
            style="opacity: .8;">
        <span class="brand-text font-weight-light">-</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('images/logo/avatar.png')}}" class="img-circle elevation-3" alt="User Image">
            </div>
            <div class="info">
                
                @if(Session::get('id_admin') == TRUE)
                    <a href="#" class="d-block">{{$admin->nama}}</a>
                    <span style="color: #c2c7d0">Admin</span>
                @elseif(Session::get('id_guru') == TRUE)
                    <a href="#" class="d-block">{{$guru->nama}}</a>
                    <span style="color: #c2c7d0">Guru</span>
                @endif
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- SESSION LOGIN ADMIN -->
                @if(Session::get('id_admin') == TRUE)
                    <li class="nav-item">
                        <a href="{{url('admin')}}" class="nav-link {{$activeMenu == 'home' ? 'active' : ''}}">
                            <i class="fa fa-home"></i>
                            <p>
                                Home
                            </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview {{$activeMenu == 'tambah-guru' ? 'menu-open' : ''}}{{$activeMenu == 'data-guru' ? 'menu-open' : ''}}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Guru
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('admin/guru/tambah')}}" class="nav-link {{$activeMenu == 'tambah-guru' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tambah Guru</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('admin/guru')}}" class="nav-link {{$activeMenu == 'data-guru' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Guru</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('admin/kecamatan')}}" class="nav-link {{$activeMenu == 'kecamatan' ? 'active' : ''}}">
                            <i class="fa fa-location"></i>
                            <p>
                                Kecamatan
                            </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview {{$activeMenu == 'tambah-sekolah' ? 'menu-open' : ''}}{{$activeMenu == 'data-sekolah' ? 'menu-open' : ''}}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-school"></i>
                            <p>
                                Sekolah
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('admin/sekolah/tambah')}}" class="nav-link {{$activeMenu == 'tambah-sekolah' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tambah Sekolah</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('admin/sekolah')}}" class="nav-link {{$activeMenu == 'data-sekolah' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Sekolah</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('admin/profile')}}" class="nav-link {{$activeMenu == 'profile' ? 'active' : ''}}">
                            <i class="fa fa-user-edit"></i>
                            <p>
                                Profile
                            </p>
                        </a>
                    </li>
                
                <!-- SESSION LOGIN GURU -->
                @elseif(Session::get('id_guru') == TRUE)
                    <li class="nav-item">
                        <a href="{{url('guru/home')}}" class="nav-link {{$activeMenu == 'home' ? 'active' : ''}}">
                            <i class="fa fa-home"></i>
                            <p>
                                Home
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('guru/kecamatan')}}" class="nav-link {{$activeMenu == 'kecamatan' ? 'active' : ''}}">
                            <i class="fa fa-location"></i>
                            <p>
                                Kecamatan
                            </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview {{$activeMenu == 'tambah-sekolah' ? 'menu-open' : ''}}{{$activeMenu == 'data-sekolah' ? 'menu-open' : ''}}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-school"></i>
                            <p>
                                Sekolah
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('guru/sekolah/tambah')}}" class="nav-link {{$activeMenu == 'tambah-sekolah' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tambah Sekolah</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('guru/sekolah')}}" class="nav-link {{$activeMenu == 'data-sekolah' ? 'active' : ''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Sekolah</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('guru/profile')}}" class="nav-link {{$activeMenu == 'profile' ? 'active' : ''}}">
                            <i class="fa fa-user-edit"></i>
                            <p>
                                Profile
                            </p>
                        </a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>
</aside>