@extends('admin.layouts.master',['activeMenu' => 'tambah-sekolah'])
@section('title', 'Tambah Sekolah')
@section('breadcrumb', 'Tambah Sekolah')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Tambah Data Sekolah Baru</h3>
                </div>
                <form role="form" action="{{url('admin/sekolah/tambah')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <select name="kecamatan_id" class="form-control" value="{{old('kecamatan_id')}}">
                                @foreach($kecamatans as $kecamatan)
                                    <option value="{{$kecamatan->id_kecamatan}}">{{$kecamatan->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Sekolah</label>
                            <input type="text" class="form-control" placeholder="Masukan Nama Sekolah" name="nama_sekolah" value="{{old('nama_sekolah')}}">
                        </div>
                        <div class="form-group">
                            <label>Alamat Sekolah</label>
                            <input type="text" class="form-control" name="alamat_sekolah" value="{{old('alamat_sekolah')}}" placeholder="Masukan alamat sekolah">
                        </div>
                        <div class="form-group">
                            <label>Lokasi</label>
                            <input type="text" class="form-control" name="lokasi" value="{{old('lokasi')}}" placeholder="Masukan lokasi">
                            <small>NB: Copy embed map pada google maps.</small>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control" cols="30" rows="5">{{old('keterangan')}}</textarea>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $('#password, #confirmation_password').on('keyup', function () {
            if ($('#password').val() == $('#confirmation_password').val()) {
                $('#message').html('Password dapat digunakan!').css('color', 'green');
            } else {
                $('#message').html('Password tidak sama!').css('color', 'red');
            }
        });
    </script>
@endsection