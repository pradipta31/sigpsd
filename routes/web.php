<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::post('login_proccess','AuthController@proccessLogin');
Route::get('logout','AuthController@logout');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/','DashboardController@index');

    //ROUTES GURU
    Route::get('guru/tambah', 'GuruController@tambahGuru');
    Route::post('guru/tambah', 'GuruController@simpanGuru');
    Route::get('guru', 'GuruController@index');
    Route::put('guru/password/{id}', 'GuruController@changePassword');
    Route::put('guru/{id}/edit', 'GuruController@updateGuru');
    Route::delete('guru/delete/{id}', 'GuruController@deleteGuru');

    // ROUTES KECAMATAN
    Route::get('kecamatan', 'KecamatanController@index');
    Route::post('kecamatan/tambah', 'KecamatanController@store');
    Route::put('kecamatan/{id}/edit', 'KecamatanController@update');
    Route::delete('kecamatan/delete/{id}', 'KecamatanController@destroy');

    //ROUTES SEKOLAH
    Route::get('sekolah/tambah', 'SekolahController@tambahsekolah');
    Route::post('sekolah/tambah', 'SekolahController@simpanSekolah');
    Route::get('sekolah', 'SekolahController@index');
    Route::put('sekolah/{id}/edit', 'SekolahController@updateSekolah');
    Route::delete('sekolah/delete/{id}', 'SekolahController@destroy');

    //ROUTES PROFILE
    Route::get('profile', 'ProfileController@index');
    Route::put('profile/{id}/edit', 'ProfileController@updateProfile');
});

Route::group(['prefix' => 'guru', 'namespace' => 'Guru'], function(){
    Route::get('/', 'AuthController@index');
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');

    Route::get('home', 'DashboardController@index');

    // ROUTES KECAMATAN
    Route::get('kecamatan', 'KecamatanController@index');
    Route::post('kecamatan/tambah', 'KecamatanController@store');
    Route::put('kecamatan/{id}/edit', 'KecamatanController@update');
    Route::delete('kecamatan/delete/{id}', 'KecamatanController@destroy');

    //ROUTES SEKOLAH
    Route::get('sekolah/tambah', 'SekolahController@tambahsekolah');
    Route::post('sekolah/tambah', 'SekolahController@simpanSekolah');
    Route::get('sekolah', 'SekolahController@index');
    Route::put('sekolah/{id}/edit', 'SekolahController@updateSekolah');
    Route::delete('sekolah/delete/{id}', 'SekolahController@destroy');

    //ROUTES PROFILE
    Route::get('profile', 'ProfileController@index');
    Route::put('profile/{id}/edit', 'ProfileController@updateProfile');
});

Route::group(['namespace' => 'Frontend'], function(){
    Route::get('/', 'HomeController@index');
    Route::get('sekolah/{id_sekolah}', 'SekolahController@show');
    Route::get('daftar-sekolah', 'SekolahController@daftarSekolah');
    Route::get('search', 'SekolahController@search');
    Route::get('cari', 'SekolahController@cari');
});
